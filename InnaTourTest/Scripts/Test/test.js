// Module pattern. Заменить на человеческий AMD
(function ($) {
    'use strict';

    var queryTimeout = 3000;
    var searchInput = $('#search-input');
    var searchButton = $('#search-button');
    var searchResults = $('#search-results');

    $(document).ready(function () {
        searchButton.click(startSearch);

        function startSearch() {
            searchResults.empty();
            addToSearchResults('Запрос выполняется...');
            queryServer();
        }

        function queryServer(name) {
            var query = searchInput.val();
            if (query === '') {
                addToSearchResults('Введите имя пользователя или его часть');
            }
            else {
                $.get('/Test/FindUsers?name=' + query)
                 .done(fillResults); 
            }
        }

        function fillResults(response) {
            if (response.waitForIt === true) {
                setTimeout(queryServer, queryTimeout);
            }
            else if (response.users && Array.isArray(response.users)) {
                var result = response.users;
                if (result.length === 0) {
                    searchResults.empty();
                    addToSearchResults('Поиск не дал результатов.')
                }
                else {
                    searchResults.empty();
                    result.map(getName).forEach(addToSearchResults);
                }
            } else {
                throw 'Unexpected server response.'
            }
        }

        function getName(user) {
            return user.Name;
        }

        function addToSearchResults(string) {
            searchResults.append('<li>' + string + '</li>');
        }
    });
})(jQuery)