﻿using InnaTourTest.Domain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Mvc;

namespace InnaTourTest.Controllers
{
    public class TestController : Controller
    {
        static readonly ConcurrentDictionary<string, List<User>> results = new ConcurrentDictionary<string, List<User>>();
        private readonly IUserGateway _userGateway;

        public TestController(IUserGateway userGateway)
        {
            _userGateway = userGateway;
        }

        // Poor man's DI. Нужно заменить на настоящий при первой возможности
        public TestController() : this(new UserGateway())
        {}

        public ActionResult Page()
        {
            return View();
        }

        [HttpGet]
        public JsonResult FindUsers(string name)
        {
            if(!results.ContainsKey(name))
            {
                // Запускаем новый запрос.
                results[name] = null; 
                HostingEnvironment.QueueBackgroundWorkItem(ct => FindUserByName(name));
                return new JsonResult() { Data = new { waitForIt = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            // Запрос в процессе, результатов пока нет.
            // Вообще, это неявная логика, но я не хочу здесь городить отдельный флаг
                if(results[name] == null)
            {
                return new JsonResult() { Data = new { waitForIt = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            List<User> result;
            results.TryRemove(name, out result);
            return new JsonResult() { Data = new { users = result }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private async Task FindUserByName(string name)
        {
            try
            {
                results[name] = await _userGateway.SearchUsers(name);
            }
            catch(Exception)
            {
                //TODO: Логирование.
                // Необработанное исключение в отдельном потоке - это не очень хорошо
            }
        }
    }
}