﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace InnaTourTest.Domain
{
    public interface IUserGateway
    {
        Task<List<User>> SearchUsers(string query);
    }
}