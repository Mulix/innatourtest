﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace InnaTourTest.Domain
{
    public class UserGateway : IUserGateway
    {
        // Неплохо было бы прикрутить fuzzy search, но не в рамках этого задания.
        public async Task<List<User>> SearchUsers(string query)
        {
            using (var context = new Context())
            {
                return await context.Users.Where(u => u.Name.Contains(query)).ToListAsync();
            }
        }
    }
}
