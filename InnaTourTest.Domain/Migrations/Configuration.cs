namespace InnaTourTest.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context context)
        {
            for (int i = 0; i < 10000; i++)
            { 
                context.Users.Add(new User { Name = Guid.NewGuid().ToString() });
            }

            context.SaveChanges();
        }
    }
}
