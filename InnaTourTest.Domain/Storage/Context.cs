﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace InnaTourTest.Domain
{
    public class Context : DbContext
    {
        public Context()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<Context>());    
        }

        public DbSet<User> Users { get; set; }
    }
}