﻿using System.ComponentModel.DataAnnotations;

namespace InnaTourTest.Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
